import { useCallback, useContext, useEffect, useState } from 'react';
import { decryptSharedKey, encryptWithKey } from '../helpers/encryption';
import { useStoreActions, useStoreState } from '../core/appStore';
import { AppMessage, AppMessageType } from '../types/AppMessage';
import { InviteInfo } from '../types/InviteInfo';
import { DeviceTokenResponsePayload, NavigationContext } from '../components/App';
import { apiRequest } from '../core/apiRequest';
import { ChannelType } from './NewChannelPage';
import InfoText from '../components/InfoText';
import { maybeRequestNotificationPermission } from '../helpers/maybeRequestNotificationPermission';
import { updateDeviceTokenInLocalStorage } from '../helpers/updateDeviceTokenInLocalStorage';
import { ChannelStorageData } from '../types/ChannelStorageData';
import AppLink from '../components/AppLink';
import { touAgreementStorageKey } from '../config';

function AcceptInvitationPage() {
	const {
		deviceToken,
		inviteInfo,
		touAgreement,
		deviceKey,
	} = useStoreState(state => state);
	const {
		setDeviceToken,
		saveChannelLocally,
		setInviteInfo,
		setTouAgreement,
	} = useStoreActions((actions) => actions);
	const history = useContext(NavigationContext);
	const [inviteAcceptResult, setInviteAcceptResult] = useState<AppMessage>();
	const [isAcceptingInvite, setIsAcceptingInvite] = useState(false);
	const [newInviteChannelName, setNewInviteChannelName] = useState('');
	const [isReadyForName, setIsReadyForName] = useState(false);
	const [validatedInviteId, setValidatedInviteId] = useState('');
	const [hideTouAgreement, setHideTouAgreement] = useState(false);
	const [showChannelNameInput, setShowChannelNameInput] = useState(false);
	const [tempDeviceToken, setTempDeviceToken] = useState('');

	const checkForNewToken = useCallback((res: Response) => {
		const newToken = res.headers.get('x-oncer-token');
		if (newToken) {
			updateDeviceTokenInLocalStorage(newToken, deviceKey);
			setDeviceToken(newToken);
		}
	}, [deviceKey, setDeviceToken]);

	useEffect(() => {
		let loadedInviteInfo: InviteInfo;
		function rememberInviteToken() {
			if (!window.location.search) {
				return;
			}
			const queryParams = new URLSearchParams(window.location.search);
			const inviteId = queryParams.get('i');
			const invitePasscode = queryParams.get('p');
			// Invite key comes from the fragment. Unlike URL params, major browsers
			// do not send the fragment to the server. When the server can't see
			// the invite key it is nigh on impossible it will be able to determine
			// the channel key which is also generated device-side and kept off the
			// server but the channel key is sent encrypted by the invite key to the
			// server as part of invites.
			const keyToDecryptChannelKey = decodeURIComponent(
				new URL(window.location.toString()).hash.substring(1)
			);
			if (inviteId && invitePasscode && keyToDecryptChannelKey) {
				loadedInviteInfo = {
					id: inviteId,
					passcode: invitePasscode,
					status: {
						type: AppMessageType.Info,
						message: 'Validating...',
					},
					keyToDecryptChannelKey: keyToDecryptChannelKey,
				} as InviteInfo
				setInviteInfo(loadedInviteInfo);
				history.replace('/', null);
			}
		}
		rememberInviteToken();
	}, [history, setInviteInfo, touAgreement]);

	useEffect(() => {
		async function doValidateInvite() {
			try {
				let _tempDeviceToken: string | null = null;
				if (!inviteInfo || !inviteInfo.id) {
					return;
				}
				if (validatedInviteId === inviteInfo.id) {
					return;
				}
				if (!deviceToken) {
					const payload = (await apiRequest('/devices', {
						method: 'POST',
					})) as DeviceTokenResponsePayload;
					const { deviceToken: newDeviceToken } = payload;
					if (!newDeviceToken) {
						throw new Error(`Invalid device auth response`);
					}
					setTempDeviceToken(newDeviceToken);
					_tempDeviceToken = newDeviceToken;
				}
				setValidatedInviteId(inviteInfo.id)
				const resolvedDeviceToken = deviceToken || _tempDeviceToken;
				if (!resolvedDeviceToken) {
					throw new Error(`No device token found`)
				}
				const {
					channelType,
					channelName,
				} = await apiRequest(`/channels/invites/${inviteInfo.id}`, {
					deviceToken: resolvedDeviceToken,
					checkForNewToken,
				}) as {
					channelType: ChannelType
					channelName: string
				}
				setInviteInfo(Object.assign({}, inviteInfo, {
					channelType,
					channelName,
					status: {
						type: AppMessageType.Success,
						message: 'Valid channel invite'
					}
				}))
				setIsReadyForName(true);
			} catch (err: any) {
				console.error(err);
				setInviteInfo(Object.assign({}, inviteInfo, {
					status: {
						type: AppMessageType.Error,
						message: err.message
					}
				}));
			}
		}
		doValidateInvite();
	}, [checkForNewToken, deviceToken, inviteInfo, setInviteInfo, validatedInviteId, touAgreement]);

	const handleGoToChannel = useCallback((newChannelId: string) => {
		function goToChannel() {
			history.push(`/channels/${newChannelId}`, {
				autoGetMessage: inviteInfo?.channelType === 'bidirectional',
				autoGetMessageTimestamp: Date.now(),
				justAcceptedInvite: true,
			});
			window.scrollTo(0, 0);
		}
		goToChannel();
	}, [history, inviteInfo?.channelType]);

	const acceptInvite = useCallback(() => {
		async function doAcceptInvite() {
			try {
				const resolvedDeviceToken = deviceToken || tempDeviceToken;
				setIsReadyForName(false);
				setIsAcceptingInvite(true);
				setInviteAcceptResult(undefined);
				if (!inviteInfo || !inviteInfo.keyToDecryptChannelKey) {
					throw new Error('Tried to accept invalid invitation')
				}
				const channelKeyPair = await window.crypto.subtle.generateKey(
					{
						name: "ECDH",
						namedCurve: "P-384",
					},
					true,
					['deriveKey']
				);
				if (!channelKeyPair.privateKey || !channelKeyPair.publicKey) {
					throw new Error(`Unable to generate device key pair`);
				}
				const exportedPrivateKey = await window.crypto.subtle.exportKey('jwk', channelKeyPair.privateKey);
				const exportedPublicKey = await window.crypto.subtle.exportKey('jwk', channelKeyPair.publicKey);
				const {
					channelPasscode: passcodeBase64,
					channelName: name,
					channelType,
					encryptedEncryptKey,
					creationTimestamp,
					updateTimestamp,
					channelId,
					originPublicKey,
				} = (await apiRequest(`/channels/invites/${inviteInfo.id}`, {
					method: 'POST',
					passcode: inviteInfo.passcode,
					deviceToken: resolvedDeviceToken,
					checkForNewToken,
					body: inviteInfo.channelType === 'unidirectional' ? {} : {
						receiverPublicKey: exportedPublicKey
					},
				})) as {
					channelId: string;
					channelName: string;
					channelPasscode: string;
					channelType: ChannelType;
					encryptedEncryptKey: string;
					creationTimestamp: number;
					updateTimestamp: number;
					originPublicKey: JsonWebKey;
				};
				if (!channelId) {
					throw new Error(`Error accepting invite: no channel id`)
				}
				maybeRequestNotificationPermission()
				const newChannelBase = {
					id: channelId,
					channelType,
					name,
					friendlyName: newInviteChannelName,
				}
				const [iv, salt, encryptedValue] = encryptedEncryptKey.split('.');
				const realChannelEncryptionKeyBase64 = await decryptSharedKey(
					encryptedValue,
					inviteInfo.keyToDecryptChannelKey,
					iv,
					salt
				);
				setIsAcceptingInvite(false);
				const newChannel = Object.assign({}, newChannelBase, {
					
					storageKey: `channel_${newChannelBase.id}`,
					creationTimestamp: creationTimestamp || (Date.now() / 1000),
					updateTimestamp: updateTimestamp || (Date.now() / 1000),
					sortOrder: Date.now(),
					recipientPublicKey: originPublicKey,
					createdByThisDevice: false,
				} as Partial<ChannelStorageData>) as ChannelStorageData;
				if (deviceKey) {
					newChannel.encryptedPrivateKey = await encryptWithKey(deviceKey, JSON.stringify(
						exportedPrivateKey
					));
					newChannel.encryptedPasscode = await encryptWithKey(deviceKey, passcodeBase64)
					newChannel.encryptedEncryptKey = await encryptWithKey(deviceKey, realChannelEncryptionKeyBase64);
				} else {
					newChannel.privateKey = exportedPrivateKey;
					newChannel.passcodeBase64 = passcodeBase64;
					newChannel.encryptKeyBase64 = realChannelEncryptionKeyBase64;
				}
				saveChannelLocally(newChannel);
				setInviteInfo(null);
				if (!deviceToken && tempDeviceToken) {
					updateDeviceTokenInLocalStorage(tempDeviceToken, null);
					setDeviceToken(tempDeviceToken);
				}
				if (!touAgreement) {
					localStorage.setItem(touAgreementStorageKey, JSON.stringify(true));
					setTouAgreement(true);
				}
				handleGoToChannel(newChannel.id);
			} catch (err: any) {
				console.error(err)
				setIsAcceptingInvite(false);
				setInviteAcceptResult({
					type: AppMessageType.Error,
					message: err.message
				})
			}
		}
		doAcceptInvite();
	}, [inviteInfo, deviceToken, checkForNewToken, newInviteChannelName, saveChannelLocally, setInviteInfo, handleGoToChannel, deviceKey]);

	return <>
		{!inviteInfo && <div className='InviteWrapper'>Invitation not found</div>}
		{!!inviteInfo && <div className='InviteWrapper'>
			<div className='Invite'>
				<h2>Accept Invite to Channel</h2>
				<div className='mb-0_5 mt-1'>
					<div className='mb-0_5'><span>
						<strong>Channel name: {inviteInfo.channelName}</strong>{' '}
						<small>
							<button
								className='linkbutton'
								onClick={() => {
									setShowChannelNameInput(!showChannelNameInput)
								}}
							>Change</button>{' '}
						</small>
					</span></div>
					{showChannelNameInput && <div>
						<div>
							<label htmlFor='newChannelName'>
								New name:
							</label>{' '}
							<InfoText>
								<small>Optional. The name should be used to remind you who shared
									the channel with you; for example, "Adrian". This name is not
									sent to the server, it is only stored on your device.</small>
							</InfoText>
						</div>
						<input
							id='newInviteChannelName'
							className='TextInput'
							disabled={!isReadyForName}
							placeholder="e.g. The name of the person who sent you the invite"
							value={newInviteChannelName}
							onChange={(event) => {
								setNewInviteChannelName(event.target.value)
							}}
						/>
					</div>}
				</div>
				<div><small>We store some data on
					your device which is required for Oncer.io to function, <AppLink
						href='/terms-of-use'>Terms of Use</AppLink> and <AppLink
							href='/privacy-policy'>Privacy Policy</AppLink>.</small></div>
				{inviteInfo.status.type !== AppMessageType.Success && <div className={`AppMessage ${AppMessageType[inviteInfo.status.type]}`}>
					{inviteInfo.status.message}
				</div>}
				{inviteAcceptResult && <div className={`AppMessage ${AppMessageType[inviteAcceptResult.type]}`}>
					{inviteAcceptResult.message}
				</div>}
				{inviteInfo.status.type === AppMessageType.Success && inviteAcceptResult?.type !== AppMessageType.Success && <div className='mt-1'>
					<button
						className='button'
						disabled={isAcceptingInvite}
						onClick={acceptInvite}>Accept Invite{
							inviteInfo.channelType === 'bidirectional' ?
								' and Get Message' :
								''}</button>
				</div>}
				{inviteInfo.status.type === AppMessageType.Success && inviteAcceptResult?.type === AppMessageType.Success && <div className='mt-0_5'>
					<button
						className='button'
						onClick={
						(event) => {
							event.preventDefault()
							setInviteInfo(null)
							history.push('/', {})
							window.scrollTo(0, 0)
						}
					}>Close &amp; go to channels</button>
				</div>}
				{!touAgreement && hideTouAgreement && <div className='mt-1'>
					<button
						className='button'
						onClick={() => {
							setHideTouAgreement(false)
							setInviteInfo(Object.assign({}, inviteInfo, {
								status: {
									type: AppMessageType.Info,
									message: 'Validating...',
								} as AppMessage
							}))
						}}
					>Try Again</button>
				</div>}
				<div className='mt-1_5 ChannelInviteMeta'>
					<div>Invite id: {inviteInfo.id}</div>
					{inviteInfo.channelType && <div className='mt-0_5'>
						Channel type: {inviteInfo.channelType && (
							inviteInfo.channelType === 'unidirectional' ?
								'Unidirectional' :
								'Bidirectional'
						)}{' '}
						{inviteInfo.channelType === 'unidirectional' && <InfoText>
							<small>
								You can send messages with this channel, but not receive messages.
							</small>
						</InfoText>}
						{inviteInfo.channelType === 'bidirectional' && <InfoText>
							<small>
								You can send and receive messages with this channel.
							</small>
						</InfoText>}
					</div>}
				</div>
			</div>
		</div>}
	</>
}

export default AcceptInvitationPage;
