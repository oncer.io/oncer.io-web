import { useState, useCallback } from 'react';
import { devicePasswordSaltKey } from '../config';
import { useStoreState, useStoreActions } from '../core/appStore';
import { emptyAppMessage } from '../core/emptyAppMessage';
import { decryptWithKey, encodeUint8ArrayAsBase64, encryptWithKey, getEncryptionKeyFromPassword, getRandomSalt } from '../helpers/encryption';
import { updateDeviceTokenInLocalStorage } from '../helpers/updateDeviceTokenInLocalStorage';
import { AppMessage, AppMessageType } from '../types/AppMessage';
import { ChannelStorageData } from '../types/ChannelStorageData';

function AccountPage() {
	const {
		channels,
		deviceKey: currentDeviceKey,
		localDeviceId,
		deviceToken,
	} = useStoreState(state => state);
	const {
		setDeviceKey,
		setDeviceKeySaltBase64,
		setUsesDevicePassword,
		updateChannelLocally,
	} = useStoreActions(actions => actions)
	const [confirmation, setConfirmation] = useState<string>('');
	const [newDevicePassword, setNewDevicePassword] = useState<string>('');
	const [passwordUpdateMessage, setPasswordUpdateMessage] = useState<AppMessage>(emptyAppMessage);
	const didConfirm = confirmation === 'DELETE';

	const clearAllData = useCallback(() => {
		localStorage.clear();
		window.location.assign('/');
	}, []);

	return <>
		<h2 className='SectionWrapper'>Account</h2>
		<form
			id='device-password-form'
			className='SectionWrapper SectionWrapperNoVerticalPadding'
			onSubmit={(event) => {
				event.preventDefault();
				async function initDevicePassword() {
					try {
						const salt = getRandomSalt();
						const newDeviceKey = await getEncryptionKeyFromPassword(
							newDevicePassword, salt
						)
						window.localStorage.setItem(devicePasswordSaltKey, encodeUint8ArrayAsBase64(salt));
						await updateDeviceTokenInLocalStorage(
							deviceToken,
							newDeviceKey
						);
						for (const channel of channels) {
							// Now that the device key in the state, the channel's private key
							// will be encrypted automatically as part of the update.
							const channelCopy = Object.assign({}, channel) as ChannelStorageData;
							if (currentDeviceKey) {
								if (!channel.encryptedPrivateKey || !channel.encryptedPasscode || !channel.encryptedEncryptKey) {
									throw new Error('Device key currently in use but keys and/or passcodes and/or encrypt keys not encrypted');
								}
								channelCopy.encryptedPrivateKey = await encryptWithKey(
									newDeviceKey,
									await decryptWithKey(
										currentDeviceKey,
										channel.encryptedPrivateKey.ivBase64,
										channel.encryptedPrivateKey.cipherBase64
									)
								)
								channelCopy.encryptedPasscode = await encryptWithKey(
									newDeviceKey,
									await decryptWithKey(
										currentDeviceKey,
										channel.encryptedPasscode.ivBase64,
										channel.encryptedPasscode.cipherBase64
									)
								)
								channelCopy.encryptedEncryptKey = await encryptWithKey(
									newDeviceKey,
									await decryptWithKey(
										currentDeviceKey,
										channel.encryptedEncryptKey.ivBase64,
										channel.encryptedEncryptKey.cipherBase64
									)
								)
							} else {
								if (!channel.privateKey || !channel.passcodeBase64 || !channel.encryptKeyBase64) {
									throw new Error('No current device key but keys and/or passcodes and/or encrypt keys not present');
								}
								channelCopy.encryptedPrivateKey= await encryptWithKey(
									newDeviceKey,
									JSON.stringify(channel.privateKey)
								);
								channelCopy.encryptedPasscode = await encryptWithKey(
									newDeviceKey,
									channel.passcodeBase64
								)
								channelCopy.encryptedEncryptKey = await encryptWithKey(
									newDeviceKey,
									channel.encryptKeyBase64
								)
							}
							delete channelCopy.privateKey;
							delete channelCopy.passcodeBase64;
							delete channelCopy.encryptKeyBase64;
							updateChannelLocally(channelCopy);
						}
						setDeviceKeySaltBase64(encodeUint8ArrayAsBase64(salt));
						setDeviceKey(newDeviceKey);
						setUsesDevicePassword(true);
						setPasswordUpdateMessage({
							type: AppMessageType.Success,
							message: 'Password successfully updated'
						});
						setNewDevicePassword('');
					} catch (err) {
						setPasswordUpdateMessage({
							type: AppMessageType.Error,
							message: (err as any)?.message || 'Unknown error occurred',
						})
						console.error(err);
					}
				}
				initDevicePassword();
			}}
		>
			<h3 className='mt-1'>{currentDeviceKey ? 'Update ' : 'Set '}Device Password</h3>
			<p className='mt-1'>
				Set a device password which will be used to encrypt all your private
				channel keys saved in device storage. If you lose this password, you
				will lose access to all your Oncer.io channels.
			</p>
			<input
				id='username'
				name='username'
				value={localDeviceId}
				type='text'
				className='display-none'
				autoComplete='username'
				readOnly={true}
			/>
			<div className='mt-1'>
				<label htmlFor='device-password'><strong>Device password</strong></label>
			</div>
			<div className='mt-0_5'>
				Enter at least 6 characters.
			</div>
			<div className='mt-0_5'>
				<input
					id='device-password'
					className='TextInput'
					type='password'
					autoComplete='new-password'
					value={newDevicePassword}
					onChange={(event) => {
						setNewDevicePassword(event.target.value)
					}}
				/>
			</div>
			<div className={`AppMessage ${AppMessageType[passwordUpdateMessage.type]} mt-0_5 mb-0_5`}>
				{passwordUpdateMessage.message}
			</div>
			<div className='mt-0_5'>
				<button
					type='submit'
					className='button'
					disabled={newDevicePassword.length < 6}
				>{currentDeviceKey ? 'Update ' : 'Set '}Password</button>
			</div>
		</form>
		{currentDeviceKey && <form
			id='remove-password-form'
			className='SectionWrapper SectionWrapperNoVerticalPadding'
			onSubmit={async (event) => {
				event.preventDefault();
				setPasswordUpdateMessage(emptyAppMessage);
				for (const channel of channels) {
					const channelCopy = Object.assign({}, channel) as ChannelStorageData;
					if (!channel.encryptedEncryptKey || !channel.encryptedPasscode || !channel.encryptedPrivateKey) {
						throw new Error('Expected encrypted encrypt key, passcode, private key for channel but at least one was not found')
					}
					channelCopy.privateKey = JSON.parse(await decryptWithKey(
						currentDeviceKey,
						channel.encryptedPrivateKey.ivBase64,
						channel.encryptedPrivateKey.cipherBase64
					)) as JsonWebKey;
					channelCopy.passcodeBase64 = await decryptWithKey(
						currentDeviceKey,
						channel.encryptedPasscode.ivBase64,
						channel.encryptedPasscode.cipherBase64
					);
					channelCopy.encryptKeyBase64 = await decryptWithKey(
						currentDeviceKey,
						channel.encryptedEncryptKey.ivBase64,
						channel.encryptedEncryptKey.cipherBase64
					);
					delete channelCopy.encryptedPrivateKey;
					delete channelCopy.encryptedPasscode;
					delete channelCopy.encryptedEncryptKey;
					updateChannelLocally(channelCopy);
					updateDeviceTokenInLocalStorage(deviceToken, null);
					window.localStorage.removeItem(devicePasswordSaltKey);
					setUsesDevicePassword(false);
					setDeviceKeySaltBase64('');
					setDeviceKey(null);
					setPasswordUpdateMessage({
						type: AppMessageType.Success,
						message: 'Device password removed'
					})
				}
			}}
		>
			<div className='mt-1'>
				<button
					type='submit'
					className='linkbutton'
				>Clear device password</button>
			</div>
		</form>}
		<form
			id='delete-account-form'
			className='SectionWrapper'
			onSubmit={(event) => {
				event.preventDefault();
				if (!didConfirm) {
					return;
				}
				clearAllData();
			}}
		>
			<h3 className='mt-1'>Delete Account</h3>
			<p className='mt-1'>
				Delete all Oncer.io data on this device. All your channels will be
				removed. This is not reversible.
			</p>
			<div className='mt-1'>
				<label htmlFor='confirmation'>Type "DELETE" to confirm</label>
			</div>
			<div className='mt-0_5'>
				<input
					className='TextInput'
					id='confirmation'
					value={confirmation}
					onChange={
						(event) => setConfirmation(event.target.value || '')
					} />
			</div>
			<div className='mt-0_5'>
				<button
					type='submit'
					className='button'
					disabled={!didConfirm}
				>Delete Account</button>
			</div>
		</form>
	</>
}

export default AccountPage;
