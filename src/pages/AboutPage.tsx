import AppLink from '../components/AppLink';

function AboutPage() {
	return <>
		<p className='SectionWrapper'>
			Never leave sensitive data hanging around in emails or messages again!
		</p>
		<div className='SectionWrapper'>
			<h2>How Oncer.io works</h2>
			<p>
				Oncer.io makes it super easy to send encrypted messages. You don't
				need to set up an account to send a message; and the person you're
				sending a message to, doesn't need to set up an account to receive it
				either!
			</p>
			<p>
				As long as you can share a link with someone, or even just show them a
				QR code, you can start sending and receiving encrypted messages.
			</p>
			<p>
				Find out more about <AppLink href='/security'>security</AppLink>.
			</p>
			<h3 className='mt-1'>See the code</h3>
			<p className='mt-0_5'>
				Web frontend: <a
					href="https://gitlab.com/oncer.io/oncer.io-web"
					rel="noopener noreferrer"
				>https://gitlab.com/oncer.io/oncer.io-web</a><br />
				Server: <a
					href="https://gitlab.com/oncer.io/oncer.io-server"
					rel="noopener noreferrer"
				>https://gitlab.com/oncer.io/oncer.io-server</a><br />
				(mirror) Web frontend: <a
					href="https://github.com/oncer.io/oncer.io-web"
					rel="noopener noreferrer"
				>https://github.com/oncer.io/oncer.io-web</a><br />
				(mirror) Server: <a
					href="https://github.com/oncer.io/oncer.io-server"
					rel="noopener noreferrer"
				>https://github.com/oncer.io/oncer.io-server</a>
			</p>
		</div>
		<div className='SectionWrapper'>
			<h2>Run Oncer.io on your own servers</h2>
			<p>
				If you want to run Oncer.io on your own servers or request
				custom features for your organisation, please get in touch!
			</p>
			<p><a
				href="mailto:questions@oncer.io?subject=Run%20Oncer.io%20On%20A%20Server"
			>questions@oncer.io</a></p>
		</div>
		<div className='SectionWrapper'>
			<h2>Integrate Oncer.io with your product</h2>
			<p>
				If you have a system where you want a convenient way to send sensitive
				data that can only viewed once, please get in touch about integrating
				it with Oncer.io!
			</p>
			<p><a
				href="mailto:integrations@oncer.io?subject=Oncer.io%20Integration"
			>integrations@oncer.io</a></p>
		</div>
		<div className='SectionWrapper'>
			<h2>Roadmap of upcoming features</h2>
			<p>
				Here are features being considered for future versions of
				Oncer.io:
			</p>
			<ul>
				<li>
					Accessibility improvements.
				</li>
				<li>
					Notifications are sent when a message is pushed to a channel.
				</li>
				<li>
					Files can be sent through a channel.
				</li>
				<li>
					When possible, send a message via an end-to-end encrypted peer-to-peer connection.
				</li>
				<li>
					Add additional payment methods.
				</li>
				<li>
					Create unlimited plan.
				</li>
				<li>
					Make all the code open-source.
				</li>
			</ul>
			<p>Do you have other ideas? Please let us know!</p>
			<p><a
				href="mailto:features@oncer.io?subject=Feature%20Request"
			>features@oncer.io</a></p>
		</div>
		<div className='SectionWrapper'>
			<h2>Bug reporting</h2>
			<p>
				Is something not working? Found an security issue? Please let us know!
				We are happy to pay for help finding and fixing issues.
            </p>
			<p><a
				href="mailto:bugs@oncer.io?subject=Bug"
			>bugs@oncer.io</a></p>
		</div>
	</>
}

export default AboutPage;
