import { useContext } from 'react';
import AppLink from '../components/AppLink';
import { useStoreActions, useStoreState } from '../core/appStore';
import HowToUseCreateChannelImage from '../images/how-to-use-oncer.io-create-channel.png';
import HowToUseShareChannelImage from '../images/how-to-use-oncer.io-share.png';
import HowToUseMessageImage from '../images/how-to-use-oncer.io-message.png';
import { SecurityTip } from '../components/SecurityTip';
import { TechnicalSecurityNotes } from '../components/TechnicalSecurityNotes';
import { touAgreementStorageKey } from '../config';
import { NavigationContext } from '../components/App';

function TouAgreementPage() {
	const { touAgreement } = useStoreState(state => state);
	const { setTouAgreement } = useStoreActions(actions => actions);
	const history = useContext(NavigationContext);

	return <div className='SectionWrapper'>
		{typeof touAgreement === 'boolean' && !touAgreement && <div>
			Error 🙃
		</div>}
		{typeof touAgreement === 'boolean' && touAgreement && <div>
			<AppLink href='/new-channel'></AppLink>
		</div>}
		<h2 className='NewChannelHeading'>
			Send passwords, secrets, and personal data in encrypted messages without
			history.
		</h2>
		<p><small>We store some data on
			your device which is required for Oncer.io to function, <AppLink
			href='/terms-of-use'>Terms of Use</AppLink> and <AppLink
				href='/privacy-policy'>Privacy Policy</AppLink>.</small></p>
		<div className='mt-2 text-align-center'>
			<button
				className='button'
				onClick={(e) => {
					e.preventDefault();
					localStorage.setItem(touAgreementStorageKey, JSON.stringify(true));
					setTouAgreement(true);
					history.push('/new-channel');
				}}
			>Start Sending Messages</button>
		</div>
		<p className='mt-2'><b>Sending encrypted messages outside of email and chat history can be easy with Oncer.io:</b></p>
		<p className='mt-1'>Step 1: create a channel</p>
		<div className='HowToScreenshot mt-1'><img src={HowToUseCreateChannelImage} alt="Screenshot depicting Oncer.io create channel screen" /></div>
		<p className='mt-1'>Step 2: share the invite via link or QR code</p>
		<div className='HowToScreenshot mt-1'><img src={HowToUseShareChannelImage} alt="Screenshot depicting Oncer.io share channel buttons" /></div>
		<p className='mt-1'>Step 3: use and re-use the channel to encrypted messages to the same recipient</p>
		<div className='HowToScreenshot mt-1'><img src={HowToUseMessageImage} alt="Screenshot depicting message input for established Oncer.io channel" /></div>
		<div className='mt-2'>
			<TechnicalSecurityNotes />
		</div>
		<div className='mt-2'>
			<SecurityTip />
		</div>
	</div>
}

export default TouAgreementPage;
