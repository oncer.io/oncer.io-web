function ChangelogPage() {
	return <>
		<div className='SectionWrapper'>
			<h2>Version 0.8.9</h2>
			<ul>
				<li>More streamlined flow when getting started</li>
			</ul>
			<h2>Version 0.8.8</h2>
			<ul>
				<li>Add contributors to html head</li>
			</ul>
			<h2>Version 0.8.7</h2>
			<ul>
				<li>UI improvement: add usage detail to front page</li>
			</ul>
			<h2>Version 0.8.6</h2>
			<ul>
				<li>UX improvement: remove notifications permission request until notifications ux is better</li>
				<li>UI improvement: update header text</li>
			</ul>
			<h2>Version 0.8.5</h2>
			<ul>
				<li>UI improvement: auto select text on focus after retrieving message</li>
			</ul>
			<h2>Version 0.8.4</h2>
			<ul>
				<li>UI improvement: increase focus on primary actions</li>
			</ul>
			<h2>Version 0.8.3</h2>
			<ul>
				<li>UI bug fix: show correct channels after deletion</li>
			</ul>
			<h2>Version 0.8.2</h2>
			<ul>
				<li>UI bug fix: prevent multiple button clicks when new channel created</li>
			</ul>
			<h2>Version 0.8.1</h2>
			<ul>
				<li>Minor unidirectional and notifications ui bug fixes and improvements</li>
			</ul>
			<h2>Version 0.8.0</h2>
			<ul>
				<li>Ability to encrypt sensitive local data with password</li>
			</ul>
			<h2>Version 0.7.1</h2>
			<ul>
				<li>Add recipient public key verification</li>
			</ul>
			<h2>Version 0.7.0</h2>
			<ul>
				<li>Notifications when messages are sent and received</li>
			</ul>
			<h2>Version 0.6.0</h2>
			<ul>
				<li>Change name to Oncer.io</li>
			</ul>
			<h2>Version 0.5.1</h2>
			<ul>
				<li className='AnimatedChangeLogItem'>Animated channel message actions</li>
			</ul>
			<h2>Version 0.5.0</h2>
			<ul>
				<li>Bug fixes and refactored API usage</li>
				<li>Privacy policy update</li>
			</ul>
			<h2>Version 0.4.0</h2>
			<ul>
				<li>Terms of Use opt-in update</li>
			</ul>
			<h2>Version 0.3.5</h2>
			<ul>
				<li>Terms of Use opt-in</li>
				<li>Password moved to advanced options</li>
			</ul>
			<h2>Version 0.3.4</h2>
			<ul>
				<li>Automatically clear data on devices that have invalid token</li>
			</ul>
			<h2>Version 0.3.3</h2>
			<ul>
				<li>Add information page about security</li>
			</ul>
			<h2>Version 0.3.2</h2>
			<ul>
				<li>Bug fix: hide invitation table on receiver channels</li>
			</ul>
			<h2>Version 0.3.1</h2>
			<ul>
				<li>Add channel invite mangement</li>
			</ul>
			<h2>Version 0.3.0</h2>
			<ul>
				<li>Enable private-public key encrypted, unidirection channels</li>
			</ul>
			<h2>Version 0.2.0</h2>
			<ul>
				<li>Add private/public key mode of encryption</li>
			</ul>
			<h2>Version 0.1.0</h2>
			<ul>
				<li>2 modes of shared key AES-GCM 256 encryption
					<ul>
						<li>Use password to generate key with PBKDF2</li>
						<li>Use a shared secret to generate the key with PBKDF2</li>
					</ul>
				</li>
			</ul>
		</div>
	</>
}

export default ChangelogPage;
