import { deviceIdStorageKey } from "../config";
import { encryptWithKey } from "./encryption";

export async function updateDeviceTokenInLocalStorage(deviceToken: string, key: CryptoKey | null) {
	try {
		if (key) {
			const encryptedTokenData = await encryptWithKey(key, deviceToken);
			window.localStorage.setItem(
				deviceIdStorageKey,
				JSON.stringify({
					encrypted: true,
					data: encryptedTokenData,
				})
			)
		} else {
			window.localStorage.setItem(deviceIdStorageKey, deviceToken);
		}
	} catch (err) {
		console.error(err);
	}
}
