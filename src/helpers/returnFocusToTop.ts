export function returnFocusToTop() {
	window.scrollTo(0, 0);
	const activeElement = document.activeElement as any;
	if (activeElement?.blur) {
		activeElement.blur();
	}
	// Reset focus to top of document, similar to if a new page was really
	// loaded.
	const logo = document.querySelector('.LogoLink') as any;
	logo.focus();
	logo.blur();
}
