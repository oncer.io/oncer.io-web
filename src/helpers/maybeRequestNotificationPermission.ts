export async function maybeRequestNotificationPermission() {
	// Disabled until user experience improved.
	// TODO for the improved experience:
	//   1. Pre-prompt that the browser will prompt for notifications permission
	//   2. Add service worker so notifications work in background
	return;
	// if (!('Notification' in window)) {
	// 	return;
	// }
	// if (
	// 	Notification.permission === 'granted' ||
	// 	Notification.permission === 'denied'
	// ) {
	// 	return;
	// }
	// const doRequestNotificationPermission = async () => {
	// 	try {
	// 		await Notification.requestPermission();
	// 	} catch (err) {
	// 		console.error(err);
	// 	}
	// }
	// doRequestNotificationPermission();
}
