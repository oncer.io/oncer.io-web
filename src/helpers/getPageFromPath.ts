import AboutPage from '../pages/AboutPage';
import AccountPage from '../pages/AccountPage';
import SecurityInformation from '../pages/SecurityInformation';
import { extractChannelIdFromPath } from '../helpers/extractChannelIdFromPath';
import SubscriptionPage from '../pages/SubscriptionPage';
import ImpressumPage from '../pages/ImpressumPage';
import TermsOfUsePage from '../pages/TermsOfUsePage';
import PrivacyNoticePage from '../pages/PrivacyPolicyPage';
import NewChannelPage from '../pages/NewChannelPage';
import ChannelPage from '../pages/ChannelPage';
import ChannelsPage from '../pages/ChannelsPage';
import NotFoundPage from '../pages/NotFoundPage';
import AcceptInvitationPage from '../pages/AcceptInvitationPage';
import ChangelogPage from '../pages/ChangelogPage';
import TouAgreementPage from '../pages/TouAgreementPage';
import { InviteInfo } from '../types/InviteInfo';
import { ChannelStorageData } from '../types/ChannelStorageData';

export function getPageFromPath(
	pathname: string,
	inviteInfo: InviteInfo | null,
	touAgreement: boolean | undefined,
	channelsMap: {
		[key: string]: ChannelStorageData;
	}
): {
	PageComponent: () => JSX.Element
	pageComponentName: string
} {
	if (pathname.startsWith('/about')) {
		return {
			PageComponent: AboutPage,
			pageComponentName: 'AboutPage',
		}
	} else if (pathname.startsWith('/security')) {
		return {
			PageComponent: SecurityInformation,
			pageComponentName: 'SecurityInformation',
		}
	} else if (pathname.startsWith('/impressum')) {
		return {
			PageComponent: ImpressumPage,
			pageComponentName: 'ImpressumPage',
		}
	} else if (pathname.startsWith('/terms-of-use')) {
		return {
			PageComponent: TermsOfUsePage,
			pageComponentName: 'TermsOfUsePage',
		}
	} else if (pathname.startsWith('/privacy-policy')) {
		return {
			PageComponent: PrivacyNoticePage,
			pageComponentName: 'PrivacyNoticePage',
		}
	} else if (pathname.startsWith('/changelog')) {
		return {
			PageComponent: ChangelogPage,
			pageComponentName: 'ChangelogPage',
		}
	} else if (pathname.startsWith('/i/') || inviteInfo) {
		// Invite page is a functional page without touAgreement because it handles
		// getting touAgreement as part of invite acceptance.
		return {
			PageComponent: AcceptInvitationPage,
			pageComponentName: 'AcceptInvitationPage',
		}
	} else if (!touAgreement) {
		// Only allow access to functional pages if touAgreement is in place
		if (!pathname || pathname === '/') {
			return {
				PageComponent: TouAgreementPage,
				pageComponentName: 'TouAgreementPage',
			}
		} else {
			return {
				PageComponent: NotFoundPage,
				pageComponentName: 'NotFoundPage',
			}
		}
	} else if (pathname.startsWith('/channels/')) {
		const channelId = extractChannelIdFromPath(pathname)
		if (channelId && channelsMap[channelId]) {
			return {
				PageComponent: ChannelPage,
				pageComponentName: 'ChannelPage',
			}
		} else {
			return {
				PageComponent: NotFoundPage,
				pageComponentName: 'NotFoundPage',
			}
		}
	} else if (pathname.startsWith('/purchase-subscription')) {
		return {
			PageComponent: SubscriptionPage,
			pageComponentName: 'SubscriptionPage',
		}
	} else if (pathname.startsWith('/new-channel')) {
		return {
			PageComponent: NewChannelPage,
			pageComponentName: 'NewChannelPage',
		}
	} else if (pathname.startsWith('/account')) {
		return {
			PageComponent: AccountPage,
			pageComponentName: 'AccountPage',
		}
	} else if (
		!Object.keys(channelsMap).length &&
		(
			!pathname ||
			pathname === '/'
		)
	) {
		return {
			PageComponent: NewChannelPage,
			pageComponentName: 'NewChannelPage',
		}
	} else {
		return {
			PageComponent: ChannelsPage,
			pageComponentName: 'ChannelsPage',
		}
	}
}
