// Code based on Node.js' `lib/internal/crypto/random.js`, subject
// to Node.js license found at:
// https://raw.githubusercontent.com/nodejs/node/master/LICENSE

const kBatchSize = 128;
const kHexDigits = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102];

let uuid: Uint8Array | undefined;
let uuidData: Uint8Array | undefined;
let uuidBatch = 0;

function getBufferedUUID() {
	if (uuidData === undefined) {
		uuidData = new Uint8Array(16 * kBatchSize);
	}

	if (uuidBatch === 0) window.crypto.getRandomValues(uuidData);
	uuidBatch = (uuidBatch + 1) % kBatchSize;
	return uuidData.slice(uuidBatch * 16, uuidBatch * 16 + 16);
}

export function getRandomUuid() {
	if (typeof window.crypto.randomUUID === 'function') {
		return window.crypto.randomUUID();
	} else {
		console.warn('Potentially insecure polyfill of window.crypto.randomUUID being used');
		if (uuid === undefined) {
			uuid = new Uint8Array(36);
			uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'.charCodeAt(0);
			uuid[14] = 52;
		}
		const uuidBuf = getBufferedUUID();
		uuidBuf[8] = (uuidBuf[8] & 0x3f) | 0x80;
		let n = 0;
		uuid[0] = kHexDigits[uuidBuf[n] >> 4];
		uuid[1] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[2] = kHexDigits[uuidBuf[n] >> 4];
		uuid[3] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[4] = kHexDigits[uuidBuf[n] >> 4];
		uuid[5] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[6] = kHexDigits[uuidBuf[n] >> 4];
		uuid[7] = kHexDigits[uuidBuf[n++] & 0xf];
		// -
		uuid[9] = kHexDigits[uuidBuf[n] >> 4];
		uuid[10] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[11] = kHexDigits[uuidBuf[n] >> 4];
		uuid[12] = kHexDigits[uuidBuf[n++] & 0xf];
		// -
		// 4, uuid[14] is set already...
		uuid[15] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[16] = kHexDigits[uuidBuf[n] >> 4];
		uuid[17] = kHexDigits[uuidBuf[n++] & 0xf];
		// -
		uuid[19] = kHexDigits[uuidBuf[n] >> 4];
		uuid[20] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[21] = kHexDigits[uuidBuf[n] >> 4];
		uuid[22] = kHexDigits[uuidBuf[n++] & 0xf];
		// -
		uuid[24] = kHexDigits[uuidBuf[n] >> 4];
		uuid[25] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[26] = kHexDigits[uuidBuf[n] >> 4];
		uuid[27] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[28] = kHexDigits[uuidBuf[n] >> 4];
		uuid[29] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[30] = kHexDigits[uuidBuf[n] >> 4];
		uuid[31] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[32] = kHexDigits[uuidBuf[n] >> 4];
		uuid[33] = kHexDigits[uuidBuf[n++] & 0xf];
		uuid[34] = kHexDigits[uuidBuf[n] >> 4];
		uuid[35] = kHexDigits[uuidBuf[n] & 0xf];

		const decoder = new TextDecoder();
		return decoder.decode(uuid);
	}
}
