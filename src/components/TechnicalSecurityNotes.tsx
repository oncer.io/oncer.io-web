import AppLink from "./AppLink";

export function TechnicalSecurityNotes() {
	return <>
		🤖 Technical security notes:
		<ul>
			<li>Oncer.io does not currently have identity verification
				built-in. Please consider this when inviting recipients to Oncer.io
				channels. For example, consider sending the link or showing the QR
				code to invite someone to a Oncer.io channel in a trusted
				environment. For example, using an end-to-end encrypted messenger app
				where you have already verified the recipient. Additionally,
				consider comparing the public key fingerprints (view via "Show
				advanced details" on Channels page) via a second channel to the
				channel you send the invite with. For example, if you sent an Oncer.io
				invite link via email, the public key fingerprints could be compared
				via a call or video call.
			</li>
			<li>Initial messages are encrypted using <a
				href="https://en.wikipedia.org/wiki/Symmetric-key_algorithm">
				symmetric-keys</a> (aka shared-key). Once an invite to the channel
				is accepted, then messages are encrypted using <a
					href="https://en.wikipedia.org/wiki/Public-key_cryptography">
					public-key</a> cryptography, which addresses some known downsides
				of symmetric-key encryption. More info on the <AppLink
					href='/security'>security</AppLink> page.</li>
		</ul>
	</>
}
