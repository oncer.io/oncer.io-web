export function SecurityTip() {
	return <>
		💡 Oncer.io aims to be reasonably secure by design but there are
		things it is dependent on for good security, including: your device,
		operating system, browser, browser extensions, and how you share
		invites. To help keep your Oncer.io messages secure, consider using
		trusted antivirus software, browsers and extensions, and keep everything
		up-to-date. Stay safe 🙂
	</>
}
