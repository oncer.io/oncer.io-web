import { StoreProvider } from 'easy-peasy';
import OtvApp from './App';
import { otvAppStore } from '../core/appStore';

function AppCore() {
	return (
		<StoreProvider store={otvAppStore}>
			<OtvApp />
		</StoreProvider>
	)
}

export default AppCore;
