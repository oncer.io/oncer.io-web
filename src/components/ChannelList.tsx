import ChannelInfoItem from './ChannelInfoItem';
import { ChannelStorageData } from '../types/ChannelStorageData';

function ChannelList(props: {
	channels: ChannelStorageData[]
}) {
	const { channels } = props;
	return <>
		{channels.map(channel => <ChannelInfoItem
			key={channel.id}
			channel={channel} />)}
	</>
}

export default ChannelList;
