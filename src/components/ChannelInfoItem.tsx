import { ChannelStorageData } from '../types/ChannelStorageData';
import AppLink from './AppLink';

function ChannelInfoItem(props: { channel: ChannelStorageData }) {
	const { channel } = props;
	const channelPath = `/channels/${channel.id}`;
	return <AppLink href={channelPath} className='ChannelInfoItemLink'>
		<div className='ChannelInfoItemLinkContent'>
			<div>
				{channel.hasMessage ? '📬' : '📭'}
			</div>
			<div>
				<div className='ChannelInfoItemName'>
					<b>{channel.friendlyName || channel.name}</b>
					{!!channel.friendlyName && <>
						{' '}
						<small>({channel.name})</small>
					</>}
				</div>
				<div className='ChannelInfoIdWrapper'>
					<small>Channel Id: {channel.id}</small>
				</div>
				<div className='ChannelInfoMetaWrapper'>
					<small>
						Updated: {new Date(channel.updateTimestamp * 1000).toLocaleString()}<br />
					</small>
				</div>
			</div>
			<div>
				<div className='ChannelInfoItemLinkIcon'>➔</div>
			</div>
		</div>
	</AppLink>
}

export default ChannelInfoItem;
