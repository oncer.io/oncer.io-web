import { useEffect, useRef } from "react";
import ReactDOM from "react-dom";

function ConfirmChannelActionModal(props: {
	onCancel: () => void
	onConfirm: () => void
	confirmButtonText: string
	title: string
	message: string
}) {
	const cancelButtonRef = useRef<HTMLButtonElement | null>(null);

	useEffect(() => {
		cancelButtonRef.current?.focus();
	}, [])

	return ReactDOM.createPortal(
		<div className='BodyShadowOverlay'>
			<form
				onSubmit={(event) => {
					event.preventDefault()
					props.onConfirm()
				}}
				className='ChannelActionConfirmContainer'
			>
				<h1 className='ChannelActionConfirmHeader'>{
					props.title
				}</h1>
				<div>{props.message}</div>
				<div className='ChannelActionButtonsContainer'>
					<button
						className='button CancelButton'
						ref={cancelButtonRef}
						onClick={(event) => {
							event.preventDefault();
							event.stopPropagation();
							props.onCancel();
						}}
					>Cancel</button>
					<button
						className='button'
						type="submit"
					>{props.confirmButtonText}</button>
				</div>
			</form>
		</div>,
		document.body
	)
}

export default ConfirmChannelActionModal;
