import React, { useContext } from 'react';
import { returnFocusToTop } from '../helpers/returnFocusToTop';
import { NavigationContext } from './App';

function AppLink(props: React.DetailedHTMLProps<
	React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement
>) {
	const { href, children, onClick, ...otherProps } = props;
	const history = useContext(NavigationContext);
	if (onClick) {
		throw new Error('Custom onClick not yet supported');
	}
	if (typeof props.href !== 'string') {
		throw new Error('Non-string hrefs not yet supported');
	}
	return <a
		href={props.href}
		{...otherProps}
		onClick={(event) => {
			event.preventDefault();
			history.push(props.href || '');
			returnFocusToTop();
		}}
	>{children}</a>
}

export default AppLink;
