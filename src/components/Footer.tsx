import AppLink from './AppLink';
import { useStoreState } from '../core/appStore';

function Footer() {
	const { activeSubscriptionId, deviceToken } = useStoreState(state => state);
	
	return <div className='Footer'>
		<div className="FooterContent">
			{!!activeSubscriptionId && <div className="mb-0_5">{<AppLink
				href="/purchase-subscription"
				>Your Oncer.io Plus Subscription</AppLink>}</div>}
			<div>
				Feedback:{' '}
				<a
					href="mailto:feedback@oncer.io?subject=Feedback"
				>feedback@oncer.io</a>.
			</div>
			<div className='mt-1_5 LegalLinks'>
				<AppLink href='/'>Home</AppLink>{' '}
				<AppLink href='/about'>About</AppLink>{' '}
				<AppLink href='/impressum'>Imprint</AppLink>{' '}
				<AppLink href='/privacy-policy'>Privacy Policy</AppLink>{' '}
				<AppLink href='/terms-of-use'>Terms of Use</AppLink>{' '}
				<AppLink href='/security'>Security</AppLink><>
					{!!deviceToken && ' '}
					{!!deviceToken && <AppLink href='/account'>Account</AppLink>}
				</>
			</div>
			<div className='WebAppVersion'>
				Version: 0.8.9{' '}
				<AppLink href='/changelog'>Changelog</AppLink>{' '}
				🐦
			</div>
		</div>
	</div>
}

export default Footer;
