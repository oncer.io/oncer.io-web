import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { io, Socket } from 'socket.io-client';
import '../styles/App.css';
import { activeSubscriptionIdStorageKey, deviceIdStorageKey, devicePasswordSaltKey, localDeviceIdStorageKey, touAgreementStorageKey } from '../config';
import { AppMessageType } from '../types/AppMessage';
import { useStoreActions, useStoreState } from '../core/appStore';
import { createBrowserHistory, Location } from "history";
import Footer from './Footer';
import AppLink from './AppLink';

import { apiRequest } from '../core/apiRequest';
import { returnFocusToTop } from '../helpers/returnFocusToTop';
import { decodeBase64AsArrayBuffer, decryptWithKey, getEncryptionKeyFromPassword } from '../helpers/encryption';
import { updateDeviceTokenInLocalStorage } from '../helpers/updateDeviceTokenInLocalStorage';
import { ChannelStorageData } from '../types/ChannelStorageData';
import { getPageFromPath } from '../helpers/getPageFromPath';
import { getRandomUuid } from '../helpers/getRandomUuid';

const browserHistory = createBrowserHistory();

export const NavigationContext = React.createContext(browserHistory);

export type DeviceTokenResponsePayload = {
  deviceToken: string;
}

function OtvApp() {
  const {
    channels,
    channelsMap,
    inviteInfo,
    deviceToken,
    activeSubscriptionId,
    touAgreement,
    deviceKeyStatus,
    deviceKey,
    localDeviceId,
    notifications,
  } = useStoreState(state => state);
  const {
    addChannel,
    setDeviceToken,
    setDeviceKey,
    setActiveSubscriptionId,
    saveActiveSubscriptionIdLocally,
    setTouAgreement,
    updateChannelLocally,
    setDeviceKeyStatus,
    setLocalDeviceId,
    setNotifications,
  } = useStoreActions((actions) => actions);
  const [location, setLocation] = useState<Location | null>(null);
  const history = useContext(NavigationContext);
  const [isLoaded, setIsLoaded] = useState(false);
  const [connectionError, setConnectionError] = useState('');
  const subscriptionTimeoutRef = useRef<null | number>(null);
  const lastTouAgreementValue = useRef<boolean | undefined>(false);
  const isGettingDeviceIdRef = useRef<boolean>(false);
  const didCheckForMessages = useRef<boolean>(false);
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const wsSocket = useRef<Socket<
    {
      messageChange: (message: {
        channelId: string;
        hasMessage: boolean;
        isFromSelf: boolean;
      }) => void,
      inviteAccepted: (message: {
        channelId: string;
        recipientPublicKey: JsonWebKey;
      }) => void
    }, {}> | null>(null);

  useEffect(() => {
    setLocation(history.location);
    const unlisten = history.listen(locationUpdate => {
      setLocation(locationUpdate.location);
    });
    return unlisten;
  }, [history]);

  const handleMessageChangeEvent = useCallback((message: {
    channelId: string;
    hasMessage: boolean;
    isFromSelf: boolean;
  }) => {
    const channel = channelsMap[message.channelId];
    if (!channel) {
      return;
    }
    if (!message.isFromSelf && message.hasMessage && 'Notification' in window && Notification.permission === 'granted') {
      const hasExistingMessageForChannel = !!notifications.find(
        notification => notification.data?.channelId === message.channelId
      );
      if (hasExistingMessageForChannel) {
        return;
      }
      const notification = new Notification(`New Message - "${channel.friendlyName || channel.name}"`, {
        data: {
          channelId: channel.id 
        }
      });
      notification.addEventListener('click', () => {
        history.push(`/channels/${message.channelId}`, {
          autoGetMessage: true,
          autoGetMessageTimestamp: Date.now(),
        });
        returnFocusToTop();
      });
      setNotifications(notifications.concat([notification]));
    }
    updateChannelLocally(Object.assign({}, channel, {
      hasMessage: message.hasMessage
    }))
  }, [channelsMap, history, notifications, setNotifications, updateChannelLocally])

  const handleInviteAcceptedEvent = useCallback((message: {
    channelId: string;
    recipientPublicKey: JsonWebKey;
  }) => {
    const channel = channelsMap[message.channelId];
    if (!channel) {
      return;
    }
    if (!channel.recipientPublicKey) {
      updateChannelLocally(Object.assign({}, channel, {
        recipientPublicKey: message.recipientPublicKey
      }))
    }
  }, [channelsMap, updateChannelLocally]);

  useEffect(() => {
    if (!deviceToken) {
      return
    }
    if (!wsSocket.current) {
      wsSocket.current = io(import.meta.env.VITE_APP_ONCER_WS_BASE || '', {
        auth: { deviceToken },
      });
      window.addEventListener('beforeunload', () => {
        if (wsSocket.current) {
          wsSocket.current.close()
        }
      })
    }
    wsSocket.current.removeAllListeners();
    wsSocket.current.on('messageChange', handleMessageChangeEvent);
    wsSocket.current.on('inviteAccepted', handleInviteAcceptedEvent);
  }, [deviceToken, handleInviteAcceptedEvent, handleMessageChangeEvent]);

  const checkForNewToken = useCallback((res: Response) => {
    const newToken = res.headers.get('x-oncer-token');
    if (newToken) {
      updateDeviceTokenInLocalStorage(newToken, deviceKey);
      setDeviceToken(newToken);
    }
  }, [deviceKey, setDeviceToken]);

  const checkDeviceTokenValidity = useCallback((tokenOverride: string = '') => {
    async function doCheckTokenValidity() {
      try {
        const tokenToCheck = tokenOverride || deviceToken;
        await apiRequest('/devices/status', {
          checkForNewToken,
          deviceToken: tokenToCheck,
        });
        setDeviceToken(tokenToCheck);
        setIsLoaded(true);
      } catch (err: any) {
        if (err.status === 401) {
          // This device is no longer authorized (probably the JWT token
          // reached its expiration date). Therefore, clear data, and start
          // from scratch
          localStorage.clear();
          window.location.reload();
        } else {
          setConnectionError('Could not connect to server.');
          throw new Error(`An error was encountered when checking device status`);
        }
      }
    }
    doCheckTokenValidity();
  }, [checkForNewToken, deviceToken, setDeviceToken])

  const decryptDeviceKey = useCallback(() => {
    async function doDecryptDeviceKey() {
      try {
        const deviceTokenData = window.localStorage.getItem(deviceIdStorageKey);
        if (!deviceTokenData) {
          throw new Error(`Token data missing`);
        }
        const saltData = window.localStorage.getItem(devicePasswordSaltKey);
        if (!saltData) {
          throw new Error('Salt data missing');
        }
        const salt = decodeBase64AsArrayBuffer(saltData);
        const encryptedData = JSON.parse(deviceTokenData) as {
          encrypted: boolean;
          data: {
            cipherBase64: string;
            ivBase64: string;
          }
        };
        const key = await getEncryptionKeyFromPassword(password, salt);
        const decryptedDeviceToken = await decryptWithKey(
          key,
          encryptedData.data.ivBase64,
          encryptedData.data.cipherBase64
        );
        setDeviceKey(key)
        setDeviceToken(decryptedDeviceToken)
        setDeviceKeyStatus('');
        checkDeviceTokenValidity(decryptedDeviceToken);
      } catch (err) {
        console.error(err);
        setPasswordError('Password invalid');
      }
    }
    doDecryptDeviceKey();
  }, [checkDeviceTokenValidity, password, setDeviceKey, setDeviceKeyStatus, setDeviceToken])

  useEffect(() => {
    if (!deviceToken) {
      return;
    }
    if (didCheckForMessages.current) {
      return;
    }
    const doCheckForMessages = async () => {
      try {
        didCheckForMessages.current = true;
        const response = (
          await apiRequest(`/devices/channelsWithMessages`, {
            deviceToken,
            checkForNewToken
          })
        ) as string[];
        const channelsWithMessages = new Set(response);
        for (const channel of channels) {
          const hasMessage = channelsWithMessages.has(channel.id);
          if (channel.hasMessage !== hasMessage) {
            updateChannelLocally(Object.assign({}, channel, {
              hasMessage
            }))
          }
        }
      } catch (err) {
        didCheckForMessages.current = false;
        console.error(err);
      }
    }
    doCheckForMessages();
  }, [deviceToken, channels, checkForNewToken, updateChannelLocally])

  const loadDeviceToken = useCallback(() => {
    if (deviceKeyStatus === 'asking for password') {
      return;
    }
    async function doLoadDeviceToken() {
      try {
        if (isGettingDeviceIdRef.current) {
          return;
        }
        isGettingDeviceIdRef.current = true;
        const localDeviceIdRaw = window.localStorage.getItem(localDeviceIdStorageKey);
        const actualLocalDeviceId = localDeviceIdRaw ?
          localDeviceIdRaw :
          getRandomUuid();
        if (localDeviceIdRaw !== actualLocalDeviceId) {
          window.localStorage.setItem(localDeviceIdStorageKey, actualLocalDeviceId)
        }
        setLocalDeviceId(actualLocalDeviceId);
        const deviceToken = window.localStorage.getItem(deviceIdStorageKey);
        if (deviceToken) {
          // Check validity of token
          if (deviceToken.startsWith('{"encrypted"')) {
            setDeviceKeyStatus('asking for password');
          } else {
            checkDeviceTokenValidity(deviceToken);
          }
        } else {
          // Get new token
          const payload = (await apiRequest('/devices', {
            method: 'POST',
          })) as DeviceTokenResponsePayload;
          const { deviceToken: newDeviceToken } = payload;
          if (!newDeviceToken) {
            throw new Error(`Invalid device auth response`);
          }
          // deviceKey is null here, because completely new account. In future
          // may ask for password first.
          updateDeviceTokenInLocalStorage(newDeviceToken, null);
          setDeviceToken(newDeviceToken);
          setIsLoaded(true);
        }
      } catch (err) {
        setConnectionError('Could not connect to server.');
        console.error(err);
        // TODO: ask user to refresh page - no device.
      }
    }
    doLoadDeviceToken();
  }, [checkDeviceTokenValidity, deviceKeyStatus, setDeviceKeyStatus, setDeviceToken, setLocalDeviceId]);

  useEffect(() => {
    if (deviceKeyStatus === 'asking for password') {
      return;
    }
    async function checkTouAgreement() {
      if (touAgreement && touAgreement !== lastTouAgreementValue.current) {
        lastTouAgreementValue.current = touAgreement;
        setIsLoaded(false);
        loadDeviceToken();
        return;
      }
      const touAgreementRaw = localStorage.getItem(touAgreementStorageKey);
      if (!touAgreementRaw) {
        setTouAgreement(undefined);
        lastTouAgreementValue.current = undefined;
        setIsLoaded(true);
        return;
      }
      const touAgreementFromStorage = JSON.parse(touAgreementRaw);
      if (typeof touAgreementFromStorage !== 'boolean') {
        setTouAgreement(undefined);
        lastTouAgreementValue.current = undefined;
        setIsLoaded(true);
        return;
      }
      if (!touAgreementFromStorage) {
        setTouAgreement(false);
        lastTouAgreementValue.current = false;
        setIsLoaded(true);
        return;
      }
      setTouAgreement(true);
      loadDeviceToken();
    }
    checkTouAgreement();
  }, [deviceKeyStatus, loadDeviceToken, setTouAgreement, touAgreement]);

  useEffect(() => {
    async function checkForActiveSubscriptionId() {
      try {
        const activeSubscriptionId = window.localStorage.getItem(activeSubscriptionIdStorageKey);
        if (activeSubscriptionId) {
          setActiveSubscriptionId(activeSubscriptionId);
        }
      } catch (err: any) {
        console.error(err)
      }
    }
    checkForActiveSubscriptionId();
  }, [setActiveSubscriptionId])

  useEffect(() => {
    // Move the channels into the app
    const storedChannelKeys = Object.keys(window.localStorage)
      .filter(key => key.startsWith('channel_'));
    storedChannelKeys.forEach(async function (channelStorageKey) {
      try {
        const channelRaw = localStorage.getItem(channelStorageKey)
        if (!channelRaw) {
          console.warn(`Channel ${channelStorageKey} was not found`)
          return;
        }
        const channel: ChannelStorageData = JSON.parse(channelRaw) as ChannelStorageData;
        addChannel(channel);
      } catch (err: any) {
        console.error(err);
      }
    });
  }, [addChannel]);

  useEffect(() => {
    let subIsLoaded = true;
    async function checkSubscriptionIsValid() {
      if (!isLoaded || !activeSubscriptionId || subscriptionTimeoutRef.current) {
        return;
      }
      try {
        const response = (
          await apiRequest(`/subscriptions/active`, {
            deviceToken,
            checkForNewToken
          })
        ) as {
          activeSubscriptionId?: string;
          timeRemaining?: number;
        };
        const {
          activeSubscriptionId: newActiveSubscriptionId,
          timeRemaining,
        } = response;
        if (subIsLoaded) {
          if (newActiveSubscriptionId && timeRemaining) {
            if (newActiveSubscriptionId !== activeSubscriptionId) {
              saveActiveSubscriptionIdLocally(newActiveSubscriptionId);
            }
            subscriptionTimeoutRef.current = window.setTimeout(
              checkSubscriptionIsValid,
              (timeRemaining * 1000)
            )
          }
          else {
            saveActiveSubscriptionIdLocally('');
          }
        }
      } catch (err: any) {
        console.error(err);
      }
    }
    checkSubscriptionIsValid();
    return () => {
      subIsLoaded = false;
      if (subscriptionTimeoutRef.current) {
        window.clearTimeout(subscriptionTimeoutRef.current);
      }
    }
  }, [activeSubscriptionId, checkForNewToken, deviceToken, isLoaded, saveActiveSubscriptionIdLocally, setActiveSubscriptionId]);

  const { PageComponent, pageComponentName } = getPageFromPath(
    location?.pathname || '',
    inviteInfo,
    touAgreement,
    channelsMap,
  );

  const showAllChannelsLink = !!channels.length && (pageComponentName !== 'ChannelsPage');

  const historyState = (history.location?.state as any);
  const isChannelDeleted = !!(historyState && historyState.isChannelDeleted);

  return (
    <div className="App">
      <header className="AppHeader">
        <div className="AppHeaderTopBar">
          <AppLink className='LogoLink' href="/">
            <div className="AppLogoWrapper">
            </div>
            <div className="AppTitleWrapper">
              <h1>Oncer.io</h1>
              <div className="AppTitleSubtitle">
                Message without history
              </div>
            </div>
          </AppLink>
          <div className='UpgradeContainer'>
            <em>Send passwords, secrets, and personal data - encrypted and without history.</em>
          </div>
          {!!channels.length && <div className='NotificationsContainer'>
            <AppLink
              href='/'
              className='linkbutton NotificationsIcon'>
                {!!channels.find(channel => channel.hasMessage) ? '📬' : '📭'}
            </AppLink>
          </div>}
        </div>
        {showAllChannelsLink && <div className="AllChannelsLinkWrapper">
          <AppLink href="/">❮ All Channels
          </AppLink></div>}
      </header>
      <main className='AppMainContent'>
        <div className='AppPageWrapper'>
          {isChannelDeleted && <div className='AppMessageWrapper'><div className={
            `AppMessage ${AppMessageType[AppMessageType.Success]
            } mt-0_5 mb-0_5`}>
            Channel deleted.
          </div></div>}
          {!isLoaded && !connectionError && deviceKeyStatus !== 'asking for password' && <div className='SectionWrapper'>Loading...</div>}
          {!isLoaded && connectionError && <div className='SectionWrapper'>
            <div className={
              `AppMessage ${AppMessageType[AppMessageType.Error]
              } mt-0_5 mb-0_5`}>
              Could not connect to server.
            </div>
          </div>}
          {!!isLoaded && PageComponent && <PageComponent />}
          {deviceKeyStatus === 'asking for password' ? <div className='SectionWrapper'>
            <form
              onSubmit={(event) => {
                event.preventDefault();
                decryptDeviceKey();
              }}
              className='text-align-center'
            >
              <input
                id='username'
                name='username'
                value={localDeviceId}
                type='text'
                className='display-none'
                autoComplete='username'
                readOnly={true}
              />
              <div className='mt-1'><label htmlFor='oncerIoPassword'><strong>Oncer.io Password</strong></label></div>
              <div className='mt-0_5'><input type='password' autoComplete='current-password' value={password} onChange={
                (event) => setPassword(event.target.value)
              } /></div>
              {passwordError && <div
                className='mt-1'
              >{passwordError}</div>}
              <div className='mt-1'>
                <button
                  type='submit'
                  className='button'
                >Submit</button>
              </div>
            </form>
          </div>: null}
        </div>
      </main>
      <Footer />
    </div>
  );
}

export default OtvApp;
