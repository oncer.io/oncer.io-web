export const apiBase = import.meta.env.VITE_APP_ONCER_API_BASE;
export const webAppBase = import.meta.env.VITE_APP_ONCER_WEB_APP_BASE;
export const deviceIdStorageKey = 'oncerDeviceId';
export const localDeviceIdStorageKey = 'oncerLocalDeviceId';
export const devicePasswordSaltKey = 'oncerDevicePasswordSalt';
export const touAgreementStorageKey = 'oncerTouAgreement';
export const activeSubscriptionIdStorageKey = 'oncerActiveSubscriptionId';
export const version = '0.8.9';
