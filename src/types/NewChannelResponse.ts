import { ChannelType } from "../pages/NewChannelPage";
import { EncryptionMode } from "./EncryptionMode";

export type NewChannelResponse = {
	id: string;
	name: string;
	channelType: ChannelType;
	creationTimestamp: number;
	updateTimestamp: number;
	hasMessage: boolean;
	passcodeBase64: string;
	encryptionMode: EncryptionMode;
}
