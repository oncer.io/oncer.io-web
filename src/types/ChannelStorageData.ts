import { ChannelType } from "../pages/NewChannelPage";
import { EncryptionMode } from "./EncryptionMode";

export type ChannelStorageData = {
	id: string;
	storageKey: string;
	name?: string;
	channelType: ChannelType;
	encryptionMode: EncryptionMode;
	friendlyName?: string;
	sortOrder?: number;
	encryptKeyBase64: string;
	encryptedEncryptKey: undefined;
	passcodeBase64: string;
	encryptedPasscode: undefined;
	creationTimestamp: number;
	updateTimestamp: number;
	privateKey: JsonWebKey;
	encryptedPrivateKey: undefined;
	recipientPublicKey?: JsonWebKey;
	lastSenderPublicKey?: JsonWebKey;
	createdByThisDevice?: boolean;
	recipientPublicKeyVerified?: boolean;
	hasMessage: boolean;
} | {
	id: string;
	storageKey: string;
	name?: string;
	channelType: ChannelType;
	encryptionMode: EncryptionMode;
	friendlyName?: string;
	sortOrder?: number;
	encryptKeyBase64: undefined;
	encryptedEncryptKey: { cipherBase64: string, ivBase64: string; };
	passcodeBase64: undefined;
	encryptedPasscode: { cipherBase64: string, ivBase64: string; };
	creationTimestamp: number;
	updateTimestamp: number;
	privateKey: undefined;
	encryptedPrivateKey: { cipherBase64: string; ivBase64: string; };
	recipientPublicKey?: JsonWebKey;
	lastSenderPublicKey?: JsonWebKey;
	createdByThisDevice?: boolean;
	recipientPublicKeyVerified?: boolean;
	hasMessage: boolean;
}
