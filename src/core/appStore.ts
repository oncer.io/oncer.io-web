import {
	Action,
	action,
	computed,
	Computed,
	createStore,
	createTypedHooks,
	thunk,
	Thunk,
} from 'easy-peasy';
import { activeSubscriptionIdStorageKey } from '../config';
import { ChannelStorageData } from '../types/ChannelStorageData';
import { InviteInfo } from '../types/InviteInfo';
import { Subscription } from '../types/Subscription';

interface StoreModel {
	channels: Computed<StoreModel, ChannelStorageData[]>,
	channelsMap: {
		[key: string]: ChannelStorageData;
	};
	addChannel: Action<StoreModel, ChannelStorageData>;
	updateChannel: Action<StoreModel, ChannelStorageData>;
	removeChannel: Action<StoreModel, ChannelStorageData>;
	saveChannelLocally: Thunk<StoreModel, ChannelStorageData>;
	removeChannelLocally: Thunk<StoreModel, ChannelStorageData>;
	subscriptions: Computed<StoreModel, Subscription[]>,
	subscriptionsMap: {
		[key: string]: Subscription;
	};
	addSubscription: Action<StoreModel, Subscription>;
	updateSubscription: Action<StoreModel, Subscription>;
	removeSubscription: Action<StoreModel, Subscription>;
	saveSubscriptionLocally: Thunk<StoreModel, Subscription>;
	removeSubscriptionLocally: Thunk<StoreModel, Subscription>;
	updateChannelLocally: Thunk<StoreModel, ChannelStorageData, CryptoKey | null>;
	activeSubscriptionId: string;
	touAgreement: undefined | boolean;
	setTouAgreement: Action<StoreModel, boolean | undefined>;
	setActiveSubscriptionId: Action<StoreModel, string>;
	saveActiveSubscriptionIdLocally: Thunk<StoreModel, string>;
	deviceToken: string;
	setDeviceToken: Action<StoreModel, string>;
	removeDeviceToken: Action<StoreModel>;
	showSubscriptionThankyou: boolean;
	setShowSubscriptionThankyou: Action<StoreModel, boolean>;
	inviteInfo: null | InviteInfo;
	setInviteInfo: Action<StoreModel, null | InviteInfo>;
	deviceKey: CryptoKey | null;
	setDeviceKey: Action<StoreModel, CryptoKey | null>;
	deviceKeySaltBase64: string;
	setDeviceKeySaltBase64: Action<StoreModel, string>;
	usesDevicePassword: boolean;
	setUsesDevicePassword: Action<StoreModel, boolean>;
	deviceKeyStatus: string;
	setDeviceKeyStatus: Action<StoreModel, string>;
	localDeviceId: string;
	setLocalDeviceId: Action<StoreModel, string>;
	notifications: Notification[];
	setNotifications: Action<StoreModel, Notification[]>;
}

export const otvAppStore = createStore<StoreModel>({
	touAgreement: false,
	setTouAgreement: action((state, touAgreement) => {
		state.touAgreement = touAgreement;
	}),
	channels: computed((state) => {
		const channels = Object.entries(state.channelsMap).map((a) => a[1]);
		channels.sort((a, b) => {
			return (b.sortOrder || 0) - (a.sortOrder || 0);
		});
		return channels;
	}),
	channelsMap: {},
	removeChannel: action((state, channel) => {
		const newChannelsMap: {
			[key: string]: ChannelStorageData;
		} = {};
		for (const channelId of Object.keys(state.channelsMap)) {
			if (channelId !== channel.id) {
				newChannelsMap[channelId] = state.channelsMap[channelId];
			}
		}
		state.channelsMap = newChannelsMap;
	}),
	addChannel: action((state, channel) => {
		state.channelsMap = Object.assign({}, state.channelsMap, {
			[channel.id]: channel
		});
	}),
	updateChannel: action((state, channel) => {
		state.channelsMap = Object.assign({}, state.channelsMap, {
			[channel.id]: channel
		})
	}),
	saveChannelLocally: thunk(async (actions, channel) => {
		const channelStorageKey = `channel_${channel.id}`;
		localStorage.setItem(channelStorageKey, JSON.stringify(channel));
		actions.addChannel(channel);
	}),
	removeChannelLocally: thunk(async (actions, channel) => {
		if (channel.storageKey) {
			localStorage.removeItem(channel.storageKey)
		}
		actions.removeChannel(channel)
	}),
	updateChannelLocally: thunk(async (actions, channel) => {
		let storedRecipientPublicKey: JsonWebKey | undefined;
		if (channel.storageKey) {
			// Workaround to ensure recipient public keys do not get lost. In place
			// until bug causing public keys to go missing is found.
			try {
				const existingItemData = localStorage.getItem(channel.storageKey);
				if (existingItemData) {
					const existingItem = JSON.parse(existingItemData) as ChannelStorageData;
					if (existingItem.recipientPublicKey) {
						storedRecipientPublicKey = existingItem.recipientPublicKey;
					}
				}
			} catch (err) {
				console.error(err);
			}
			if (!channel.recipientPublicKey) {
				channel.recipientPublicKey = storedRecipientPublicKey;
			}
			localStorage.setItem(channel.storageKey, JSON.stringify(channel));
		}
		actions.updateChannel(channel);
	}),
	deviceToken: '',
	setDeviceToken: action((state, deviceToken) => {
		state.deviceToken = deviceToken;
	}),
	removeDeviceToken: action((state) => {
		state.deviceToken = '';
	}),
	subscriptions: computed((state) => {
		const subscriptions = Object.entries(state.subscriptionsMap).map((s) => s[1]);
		return subscriptions;
	}),
	subscriptionsMap: {},
	removeSubscription: action((state, subscription) => {
		delete state.subscriptionsMap[subscription.id];
	}),
	addSubscription: action((state, subscription) => {
		state.subscriptionsMap[subscription.id] = subscription;
	}),
	updateSubscription: action((state, subscription) => {
		state.subscriptionsMap[subscription.id] = subscription;
	}),
	saveSubscriptionLocally: thunk(async (actions, subscription) => {
		const subscriptionStorageKey = `subscription_paypal_${subscription.id}`;
		localStorage.setItem(subscriptionStorageKey, JSON.stringify(subscription));
		actions.addSubscription(subscription);
	}),
	removeSubscriptionLocally: thunk(async (actions, subscription) => {
		if (subscription.storageKey) {
			localStorage.removeItem(subscription.storageKey);
		}
		actions.removeSubscription(subscription);
	}),
	activeSubscriptionId: '',
	setActiveSubscriptionId: action((state, activeSubscriptionId) => {
		state.activeSubscriptionId = activeSubscriptionId;
	}),
	saveActiveSubscriptionIdLocally: thunk(async (actions, activeSubscriptionId) => {
		localStorage.setItem(activeSubscriptionIdStorageKey, activeSubscriptionId);
		actions.setActiveSubscriptionId(activeSubscriptionId)
	}),
	showSubscriptionThankyou: false,
	setShowSubscriptionThankyou: action((state, showSubscriptionThankyou) => {
		state.showSubscriptionThankyou = showSubscriptionThankyou;
	}),
	inviteInfo: null,
	setInviteInfo: action((state, inviteInfo) => {
		state.inviteInfo = inviteInfo;
	}),
	deviceKey: null,
	setDeviceKey: action((state, deviceKey) => {
		state.deviceKey = deviceKey
	}),
	usesDevicePassword: false,
	setUsesDevicePassword: action((state, useDevicePassword) => {
		state.usesDevicePassword = useDevicePassword;
	}),
	deviceKeyStatus: '',
	setDeviceKeyStatus: action((state, status) => {
		state.deviceKeyStatus = status;
	}),
	deviceKeySaltBase64: '',
	setDeviceKeySaltBase64: action((state, salt) => {
		state.deviceKeySaltBase64 = salt;
	}),
	localDeviceId: '',
	setLocalDeviceId: action((state, localDeviceId) => {
		state.localDeviceId = localDeviceId;
	}),
	notifications: [],
	setNotifications: action((state, notifications) => {
		state.notifications = notifications
	}),
});

const typedHooks = createTypedHooks<StoreModel>();

export const useStoreActions = typedHooks.useStoreActions;
export const useStoreDispatch = typedHooks.useStoreActions;
export const useStoreState = typedHooks.useStoreState;
